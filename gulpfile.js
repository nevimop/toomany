var gulp = require('gulp');
var less = require('gulp-less');
var postcss = require('gulp-postcss');
var nodemon = require('gulp-nodemon');
var autoprefixer = require('autoprefixer');

gulp.task('less', function() {
  return gulp.src('./public/app/assets/index.less')
    .pipe(less({}))
    .pipe(postcss([
      autoprefixer({browsers: ['last 1 version']})
    ]))
    .pipe(gulp.dest('./public/app/assets/'));
});

gulp.task('watch', function() {
  gulp.watch(['./public/app/assets/**/*.less'], ['less']);
});

gulp.task('develop', function() {
  var nodeArgs = ['--harmony'];
  if (gulp.env.debug) {
    nodeArgs.push('--debug')
  }
  nodemon({
      script: 'app.js',
      ext: 'html js',
      ignore: ['node_modules/*'],
      nodeArgs: nodeArgs,
      env: {
        'NODE_ENV': 'development'
      }
    })
    .on('restart', function() {
      console.log('restarted!')
    });
});

//开发时使用
gulp.task('default', [
  'less',
  'develop',
  'watch'
]);