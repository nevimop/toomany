define(
  [
    'app/models/model',
    'magix'
  ],
  function(Model, Magix) {
    var M = Magix.Manager.create(Model);

    M.registerModels([

    ]);

    return M;
  });