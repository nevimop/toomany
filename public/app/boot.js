(function() {
  var script = function() {
    var scripts = document.getElementsByTagName('script')
    return scripts[scripts.length - 1]
  }();

  /*var base = function() {
    var src = script.getAttribute('src')
    var base = /(.+\/)(.+\/.+)/.exec(src)[1]
    return base
  }();*/

  require.config({
    paths: {
      app: 'app/',
      magix: '//g.alicdn.com/thx/magix/2.0/requirejs-magix'
    }
  });

  require(['magix', 'jquery'], function(Magix, $) {
    Magix.start({
      error: function(e) {
        if (console) {
          console.error(e.stack) //将错误抛出来
        }
      },
      iniFile: 'app/ini' //配置在ini.js里
    });

    //首次启动 调用登陆接口获取登陆相关信息
    // var loginMessageUrl = '/cps/shopkeeper/loginMessage.json';

    /*$.ajax({
      url: loginMessageUrl,
      type: 'post',
      async: false,
      dataType: 'json',
      success: function(resp, status, xhr) {
        $.extend(window.UserInfo, resp.data);

        // 未登录则跳转到统一登陆页
        if (resp.data.noLogin) {
          window.location.href = resp.data.loginUrlPrefix + encodeURIComponent(window.location.href);
        }

        if (!resp.data.isInCPAWhiteList) {
          window.location.href = '#!/user/join_magicbox';
        }
      }
    })*/
  })
})()
