define(
  'app/views/home/index', [
    'magix'
  ],
  function(Magix) {
    return Magix.View.extend({
      render: function() {
        var me = this;
        me.setViewHTML();
      }
    });
  });