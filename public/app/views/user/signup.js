define(
  'app/views/user/signup', [
    'magix',
    'brix/loader',
    'brix/loader/util',
  ],
  function(Magix, Loader, _) {
    return Magix.View.extend({
      render: function() {
        var me = this;
        me.setViewHTML();
      },
      upload: function(e) {
        var me = this;
        var file = e.target.files && e.target.files[0];
      },
      signup: function() {
        var me = this;
        var valid = Loader.query($('.signup_form'))[0];
        if (!valid.isValid()) {
          return false;
        }

        var params = _.extend({}, _.unparam($('.signup_form').serialize()));
        params.name = decodeURIComponent(params.name);

      }
    });
  });